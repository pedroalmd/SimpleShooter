
# Simple Shooter

●	A third person shooter demo done in Unreal Engine 5 with C++, UE5’s animation graphs, decision tree.

●	Focus on shooting mechanics, AI chasing the player and shooting on sight and investigating last known player location when they lost sight of the player.

●	Small UI implementation crosshair and health bar.

●	Alignment of animations with action speed in game for jumping, moving and aiming.

●	Soundscape for gun firing sounds and gun impact sounds.

● This demo was developed as part of the Unreal Engine 5 Udemy Course I took.








# Screenshots

![](https://gitlab.com/pedroalmd/SimpleShooter/-/raw/main/Screenshots/Animation.gif?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/SimpleShooter/-/raw/main/Screenshots/Screenshot%202023-10-07%20174125.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/SimpleShooter/-/raw/main/Screenshots/Screenshot%202023-10-07%20202117.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/SimpleShooter/-/raw/main/Screenshots/Screenshot%202023-10-07%20202154.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/SimpleShooter/-/raw/main/Screenshots/Screenshot%202023-10-07%20202303.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/SimpleShooter/-/raw/main/Screenshots/Screenshot%202023-10-07%20202454.png?ref_type=heads)


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/pedroalmd/SimpleShooter
```

Go to the project directory

```bash
  /SimpleShooter
```

Run executable and enjoy the demo!

```bash
  SimpleShooter.exe
```





## Objective

- Defeat all enemies in the level!


